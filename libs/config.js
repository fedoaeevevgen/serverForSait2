var config = {};
const ExtractJwt = require("passport-jwt").ExtractJwt;

//Статика которая отвечает за стандартные настройки
config.port = 80;
config.name = "Женя бест";
config.home = ""
config.ip="127.0.0.1"
config.http="http"


config.jwt= {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('bearer'),

    // Секретный ключ
    secretOrKey: '7DDJSu8UAvwfzRwjNgB5K4q4',
    // secretOrKey: 'secret',

    // Время жизни токена
    expiresIn: 60 * 30 * 1000,

    expiresInRefreshToken: 1000 * 60*60*24*90,
},
config.session = {
    "secret":"SashaBest",
    "key":"sid",
    "cookie":{
        "httpOnly":true,
        "maxAge":1000*60*60    
    }
};

config.mongoose = {
    uri: "mongodb://127.0.0.1/superblog",
    options: {
        server: {
            socketOptions: {
                keepAlive: 1
            }
        }
    }
};


module.exports=config;