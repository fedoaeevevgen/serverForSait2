const log = require('./../libs/log')(module);
const config = require('./../libs/config');
const jwt = require('jsonwebtoken');



module.exports = async function(app){
    app.get('/', require('./main'));
    app.post('/sing_up',require('./sing_up'));
    app.post('/sing_in',require('./sing_in'));
    app.post('/test',verify,require('./test'));
    app.post('/pay',require('./pay'));
}

function verify(req, res, next){
    try{
        jwt.verify(req.body.token, config.jwt.secretOrKey, function (err, decoded) {
            if (decoded !== undefined){
                next()
            } else  if (err) {
                res.status(401).send('Unauth');
            }
        });
    } catch (err) {
        if (err.message === "Cannot read property 'refreshToken' of null"){
            res.status(401).send('Вы не авторизированны');
        } else {
            log.error(err);
        }
    }
}