const User = require('./../db/Schma');
const jwt = require('jsonwebtoken');
const config = require('./../libs/config');
const log = require('./../libs/log')(module);

module.exports = function (req, res) {
    console.log('qq');
    console.log(req.body);
    User.findOne({email: req.body.email}).exec()
        .then(payload => {
            // Проверка на существование пользователя
            if (payload === null) {
                throw Error('Такая электронная почта не зарегистрирована!');
            } else

            // Проверка пароля
            if (payload.encryptPassword(req.body.password) !== payload.hashPassword) {
                throw Error('Неверный пароль!');
            } else
                return payload;
        })
        .then(payload => {
            // REFRESH TOKEN
            const refreshToken = jwt.sign({type: 'refresh'}, config.jwt.secretOrKey, { expiresIn: config.jwt.expiresInRefreshToken });
            User.update({ email: payload.email }, { $set: {token: refreshToken} }, (err) => {
                if (err) log.error(err);
            });

            // ACCESS TOKEN
            const tokenInfo = {
                _id: payload._id,
                name: payload.name,
                email: payload.email,
                token: refreshToken
            };

            // Шифруем
            const token = jwt.sign(tokenInfo, config.jwt.secretOrKey, {expiresIn: config.jwt.expiresIn});
            res.header('Content-Type', 'application/json');
            res.status(200).send({ token: token });
        })
        .catch(err => {
            res.header('Content-Type', 'text/plain');
            res.status(400).send(err.message);
        });

};